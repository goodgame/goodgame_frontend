import React from "react";

/* Local libraries */
import {Row} from "../../components/reactstrapGridSystem/Row";
import {Col} from "../../components/reactstrapGridSystem/Col";
import Article from "../../components/article/Article";

import articleList from "./articleList.json";

/**
 * TODO Pagination system
 */
class ArticlesList extends React.Component {

    render() {
        return (
            <>
                {articleList.articles.map((article, index) => {
                    return (
                        <Row key={`article-${index}`}>
                            <Col>
                                <Article article={article}/>
                            </Col>
                        </Row>
                    );
                })}
            </>
        );
    };
}

export default ArticlesList;